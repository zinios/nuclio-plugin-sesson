<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\session
{
	interface CommonSessionInterface
	{
		public function get(string $key):mixed;
		
		public function set(string $key,mixed $val):this;
		
		public function delete(string $key):this;
	}
}
