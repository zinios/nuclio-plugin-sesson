<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\session
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\http\request\Request;
	use nuclio\plugin\database\orm\ORM;
	use nuclio\plugin\database\datasource\manager\Manager as DataSourceManager;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\session\model\Session;
	use nuclio\plugin\session\SessionException;

	<<singleton>>
	class DatabaseSession extends Plugin implements SessionHandler
	{
		/**
		 * Handle the session management using the database
		 * 
		 * This class provides CRUD for sussions over the database
		 **/
		const string LOCATION 	= 	'/tmp/';
		const string SESSION_NAME = 'ns_session';

		private string 				$sessionId;
		private int 				$expireTime = 3600;

		private ?Session 			$session;
		private Request 			$request;
		
		/**
		 * Get the instance.
		 *
		 * @access public
		 * 
		 * @param      ...     $args 
		 *
		 * @return     DatabaseSession  Instance Object.
		 */
		public static function getInstance(/* HH_FIXME[4033] */...$args):DatabaseSession
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		/**
		 * Class constructor it will look for the session id in the database,
		 * if exist, it will create new session for the user with the same ns_id.
		 * also the constructor allow to se up expiery time for the session 
		 * 
		 * @access public
		 *
		 * @param      string  $driver      Storage driver or database connection
		 * @param      string  $sessionId   Use the session ID to check if the session exist
		 * @param      ?int    $expireTime  set expiry date for the session
		 */
		public function __construct(string $driver, string $sessionId, ?int $expireTime)
		{
			parent::__construct();
			$this->sessionId = $sessionId;
			$this->request = Request::getInstance();

			$ORM=ORM::getInstance();
			$ORM->bindDataSource(DataSourceManager::getSource($driver));
			$this->sessionId = $sessionId;
			$this->session = Session::findOne(Map{'ns_id'=>$sessionId});
			if (is_null($this->session))
			{
				$this->insertNewSession($sessionId);
			}
			if(!is_null($expireTime))
			{
				$this->expireTime = $expireTime;
			}		
		}
		
		/**
		 * Check if there is a value for a specific part of the session
		 *
		 * @access public
		 * 
		 * @param      string  $key  session key part 
		 *
		 * @return     bool  return true if the user has session
		 */
		public function _isset(string $key):bool
		{
			return !is_null($this->getVar($key));
		}
		
		/**
		 * Clear a part of the session information by setting it to null
		 * 
		 * @access public
		 *
		 * @param      string  $key   set up the key for the information required to be deleted
		 *
		 * @return     bool  session deleted
		 */
		public function delete(string $key):bool
		{
			$this->session->{'set'.ucfirst($key)}(NULL);
			$this->session->save();
			return true;
		}
		
		/**
		 * Set or update a specific part in the session with new information
		 * 
		 * @access public
		 *
		 * @param      string  $key    to access specific part of the session information
		 * @param      mixed   $val    give a value for the specific session part
		 *
		 * @return     bool  return true if information has been updated
		 */
		public function setVar(string $key, mixed $val):bool
		{
			if(substr($key, 0, 3 ) === 'ns_')
			{
				throw new SessionException('Reserved key.');
			}
			$this->session->{'set'.ucfirst($key)}($val);//This is wrong. Need to use setDynamic which needs to be moved into core first.
			$this->session->save();
			return true;
		}
		
	
		/**
		 * Get the value of a specific variable in the session
		 * 
		 * @access public
		 *
		 * @param      string  $key to access the data in the session
		 *
		 * @return     mixed  session specific part information.
		 */
		public function getVar(string $key):mixed
		{
			return $this->session->{'get'.ucfirst($key)}();
		}
		
		
		/**
		 * Return all the session information as a map
		 * 
		 * @access public
		 *
		 * @return    Map<string,mixed> Map contains all session information.
		 */
		public function getAllVars():Map<string,mixed>
		{
			return $this->session->toMap();
		}
		
		/**
		 * Delete session and remove it from the cookies 
		 * 
		 * @access public
		 *
		 * @return     bool  return true if the session is deleted
		 */
		public function destroy():bool
		{
			$return = Session::delete(Map{'_id'=>$this->sessionId});
			if($return)
			{
				setcookie(self::SESSION_NAME, 'expired', 1);
			}
			return $return;
		}
		
		/**
		 * Add new session into the database
		 * 
		 * @access public
		 *
		 * @param      string   $key  to give the session ID  (Ns_Id)
		 *
		 * @return     bool   true if new session is created
		 */
		public function insertNewSession(string $key):bool
		{
			$session = Session::create();
			$session->setNs_id($key);
			$session->setIp((string)$this->request->getServer('REMOTE_ADDR'));
			$session->setTouchTime(time());
			$session->save();
			if(!is_null($id = $session->getId()))
			{
				$this->sessionId = $key;
				$this->session = $session;
				return true;	
			}
			else
			{
				return false;
			}
		}
		
		/**
		 * Determine if the session expired.
		 * 
		 * @access public
		 *
		 * @param      int|integer  $expireTime  set expiry time
		 *
		 * @return     bool      True if expired, False otherwise.
		 */
		public function isExpired(int $expireTime=0):bool
		{
			$return=false;
			if($expireTime==0)
			{
				$expireTime = $this->expireTime;
			}
			$time = (int)$this->session->getTouchTime();
			if(is_null($time))
			{
				throw new SessionException('No time key set');
			}
			return $time+$expireTime<time();
		}
		
		/**
		 * Set the session touch time
		 * 
		 * @access public
		 *
		 * @return     bool  update completed
		 */
		public function updateTouched():bool
		{
			$this->session->setTouchTime(time());
			$this->session->save();
			return true;
		}

		/**
		 * Get the expire time.
		 * 
		 * @access public
		 *
		 * @return     int  Expire time.
		 */
		public function getExpireTime():int
		{
			return $this->expireTime;
		}
	}
}
