<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\session\model
{ 
	use nuclio\plugin\database\orm\DynamicModel;
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection session
	 */
	class Session extends DynamicModel
	{
		/**
		 * @Id(strategy="AUTO")
		 */
		public mixed $id=null;
	}
}
