<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\session
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\http\request\Request;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\fileSystem\reader\FileReader;
	use nuclio\plugin\fileSystem\writer\FileWriter;
	use nuclio\plugin\fileSystem\folder\Folder;
	use nuclio\plugin\format\driver\json\JSON;
	use nuclio\plugin\session\SessionException;

	<<singleton>>
	class FileSession extends Plugin implements SessionHandler
	{
		/**
		 * Handle the session management using the file
		 * 
		 * This class provides CRUD for sussions over the file
		 **/
		const string LOCATION 	= 	'/tmp/';
		const string SESSION_NAME = 'ns_session';

		private string 				$sessionId;
		private int 				$expireTime = 3600;

		private CommonInterface 	$driver;
		private string 				$driverName;

		private Request 			$request;
		private FileReader			$reader;
		private FileWriter			$writer;
		private JSON 				$json;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):FileSession
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		/**
		 * Class constructor it will look for the session id in the database,
		 * if exist, it will create new session for the user with the same ns_id.
		 * also the constructor allow to se up expiery time for the session 
		 * 
		 * @access public
		 *
		 * @param      string  $driver      Storage driver or database connection
		 * @param      string  $sessionId   Use the session ID to check if the session exist
		 * @param      ?int    $expireTime  set expiry date for the session
		 */
		public function __construct(string $driver, string $sessionId, ?int $expireTime)
		{
			parent::__construct();
			$this->sessionId	=$sessionId;
			$this->request		=Request::getInstance();
			$tmpDriver			=ProviderManager::request($driver);
			if ($tmpDriver instanceof CommonInterface)
			{
				$this->driver 		=$tmpDriver;
				$this->driverName 	=$driver;
				$this->json 		=JSON::getInstance();
				$this->writer 		=FileWriter::getInstance($driver, self::LOCATION.$sessionId.'.json');
				$this->reader 		=FileReader::getInstance($driver, self::LOCATION.$sessionId.'.json');
				
				if ($this->reader->read()=='')
				{
					$this->insertNewSession($sessionId);
				}
			}
			else
			{
				throw new SessionException(sprintf('Could not find driver "%s".',$driver));
			}
			if (!is_null($expireTime))
			{
				$this->expireTime = $expireTime;
			}		
		}
		
		/**
		 * Check if there is a value for a specific part of the session
		 *
		 * @access public
		 * 
		 * @param      string  $key  session key part 
		 *
		 * @return     bool  return true if the user has session
		 */
		public function _isset(string $key):bool
		{
			$map = $this->json->decode($this->reader->read());
			
			if(!$map instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			return $map->containsKey($key);
		}
		
		/**
		 * Clear a part of the session information by setting it to null
		 * 
		 * @access public
		 *
		 * @param      string  $key   set up the key for the information required to be deleted
		 *
		 * @return     bool  session deleted
		 */
		public function delete(string $key):bool //the key/val?
		{
			$map=$this->json->decode($this->reader->read());
			if(!$map instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			$map->removeKey($key);								//check if key exists?
			$this->writer->write(json_encode($map));			//better way?
			return true;
		}

		/**
		 * Set or update a specific part in the session with new information
		 * 
		 * @access public
		 *
		 * @param      string  $key    to access specific part of the session information
		 * @param      mixed   $val    give a value for the specific session part
		 *
		 * @return     bool  return true if information has been updated
		 */
		public function setVar(string $key, mixed $val):bool 	//the key/val?
		{
			$map = $this->json->decode($this->reader->read());
			if(!$map instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			if($map->containsKey($key))
			{
				$map->set($key, $val);
			}
			else
			{
				$map->add(Pair{$key, $val});
			}
			$this->writer->write(json_encode($map));			//better way?
			return true;
		}
		
		/**
		 * Get the value of a specific variable in the session
		 * 
		 * @access public
		 *
		 * @param      string  $key to access the data in the session
		 *
		 * @return     mixed  session specific part information.
		 */
		public function getVar(string $key):mixed
		{
			$map = $this->json->decode($this->reader->read());
			
			if(!$map instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			if($map->containsKey($key))
			{
				return $map->get($key);
			}
			else
			{
				return null;
			}
		}
		
		
		/**
		 * Return all the session information as a map
		 * 
		 * @access public
		 *
		 * @return    Map<string,mixed> Map contains all session information.
		 */
		public function getAllVars():Map<string,mixed>
		{
			$map = $this->json->decode($this->reader->read());
			if(!$map instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			return $map;	
		}
		
		/**
		 * Delete session and remove it from the cookies 
		 * 
		 * @access public
		 *
		 * @return     bool  return true if the session is deleted
		 */
		public function destroy():bool
		{
			$return = FileWriter::getInstance($this->driver, self::LOCATION.$this->sessionId.'.json')->delete(0);
			if($return)
			{
				setcookie(self::SESSION_NAME, 'expired', 1);
			}
			return $return;
		}
		
		/**
		 * Add new session into the database
		 * 
		 * @access public
		 *
		 * @param      string   $key  to give the session ID  (Ns_Id)
		 *
		 * @return     bool   true if new session is created
		 */
		public function insertNewSession(string $key):bool
		{
			$return = false;
			$this->reader = FileReader::getInstance($this->driverName, self::LOCATION.$key.'.json');
			$this->writer = FileWriter::getInstance($this->driverName, self::LOCATION.$key.'.json');
			$return = $this->writer->write
			(
				'{
					"ip":"'.(string)$this->request->getServer('REMOTE_ADDR').'",
					"time":"'.time().'"
				}'
			);
			$this->sessionId = $key;
			return $return;
		}
		

		/**
		 * Determine if the session expired.
		 * 
		 * @access public
		 *
		 * @param      int|integer  $expireTime  set expiry time
		 *
		 * @return     bool      True if expired, False otherwise.
		 */
		public function isExpired(int $expireTime=0):bool
		{
			$return=false;
			if($expireTime==0)
			{
				$expireTime = $this->expireTime;
			}
			$content = $this->json->decode($this->reader->read());
			if(!$content instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			if($content->containsKey('time'))
			{
				$touched = (int)$content->get('time');
				return $touched+$expireTime<time();
			}
			else
			{
				throw new SessionException("No time key set");	
			}
		}
		
		/**
		 * Set the session touch time
		 * 
		 * @access public
		 *
		 * @return     bool  update completed
		 */
		public function updateTouched():bool
		{
			$content = $this->json->decode($this->reader->read());
			if(!$content instanceof Map)
			{
				throw new SessionException('Wrong format.');
			}
			if($content->containsKey('time'))
			{
				$content->set('time', time());	
			}
			else
			{
				$content->add(Pair{'time', time()});
			}
			return (bool)$this->writer->write(json_encode($content));
		}

		/**
		 * Get the expire time.
		 * 
		 * @access public
		 *
		 * @return     int  Expire time.
		 */
		public function getExpireTime():int
		{
			return $this->expireTime;
		}
	}
}
