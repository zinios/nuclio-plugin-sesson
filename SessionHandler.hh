<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\session
{
	interface SessionHandler
	{	
		/**
		 * Check if there is a value for a specific part of the session
		 *
		 * @access public
		 * 
		 * @param      string  $key  session key part 
		 *
		 * @return     bool  return true if the user has session
		 */
		public function _isset(string $key):bool;
		
		/**
		 * Clear a part of the session information by setting it to null
		 * 
		 * @access public
		 *
		 * @param      string  $key   set up the key for the information required to be deleted
		 *
		 * @return     bool  session deleted
		 */
		public function delete(string $key):bool;
		
		/**
		 * Set or update a specific part in the session with new information
		 * 
		 * @access public
		 *
		 * @param      string  $key    to access specific part of the session information
		 * @param      mixed   $val    give a value for the specific session part
		 *
		 * @return     bool  return true if information has been updated
		 */
		public function setVar(string $key, mixed $val):bool;
		
		/**
		 * Get the value of a specific variable in the session
		 * 
		 * @access public
		 *
		 * @param      string  $key to access the data in the session
		 *
		 * @return     mixed  session specific part information.
		 */
		public function getVar(string $key):mixed;
		
		/**
		 * Return all the session information as a map
		 * 
		 * @access public
		 *
		 * @return    Map<string,mixed> Map contains all session information.
		 */
		public function getAllVars():Map<string,mixed>;
		
		/**
		 * Delete session and remove it from the cookies 
		 * 
		 * @access public
		 *
		 * @return     bool  return true if the session is deleted
		 */		
		public function destroy():bool;
		
		/**
		 * Add new session into the database
		 * 
		 * @access public
		 *
		 * @param      string   $key  to give the session ID  (Ns_Id)
		 *
		 * @return     bool   true if new session is created
		 */
		public function insertNewSession(string $key):bool;
		
		/**
		 * Determine if the session expired.
		 * 
		 * @access public
		 *
		 * @param      int|integer  $expireTime  set expiry time
		 *
		 * @return     bool      True if expired, False otherwise.
		 */
		public function isExpired(int $expireTime=0):bool;
		
		/**
		 * Set the session touch time
		 * 
		 * @access public
		 *
		 * @return     bool  update completed
		 */
		public function updateTouched():bool;

		/**
		 * Get the expire time.
		 * 
		 * @access public
		 *
		 * @return     int  Expire time.
		 */
		public function getExpireTime():int;
	}
}
