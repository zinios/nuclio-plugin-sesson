<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\session
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\
	{
		http\request\Request,
		session\SessionHandler,
		session\FileSession,
		session\DatabaseSession,
		session\SessionException
	};
	
	<<
		provides('session::fileSystem','session::database'),
		singleton
	>>
	class Session extends Plugin implements CommonSessionInterface
	{

		/**
		 * The session plugin requires, if the storage sesssion type is used, the existance of the folder /tmp/nuclio/cache/session,
		 * and for that folder to be writable by the user running this code.
		 */
		const string SESSION_NAME = 'ns_session';

		private ?string 			$_sessionId;
		private bool 				$sessionStarted	= false;
		private Map<string,mixed>	$__sessionKey	= Map
		{
			'length'=>64,
			'chars'=>'abcdefghijklmnopqrstuvwxyz0123456789'
		};

		private Request 			$request;
		private SessionHandler 		$handler;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Session
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		/**
		 * Sets the session id and creates the handler based on the passed session type
		 * @param string   $type       session handler type. Supported: 'storage'
		 * @param string   $driver     storage driver or database connection. Supported: 'fileSystem:local-disk', 'fileSystem:s3'
		 * @param int|null $expireTime optional session expiration time. Default 3600 (1 hour).
		 */
		public function __construct(Map<string,mixed> $config, ?int $expireTime=null)
		{
			parent::__construct();
			$this->request = Request::getInstance();
			$this->_sessionId = $this->getSessionId();
			$type=$config->get('type');
			$driver=$config->get('driver');
			
			if($type=='fileSystem')
			{
				$this->handler = FileSession::getInstance($driver, $this->_sessionId, $expireTime);
			}
			elseif($type=='database')
			{
				$this->handler = DatabaseSession::getInstance($driver, $config->get('dataSource'), $this->_sessionId, $expireTime);
			}
			else
			{
				throw new SessionException("By default, only fileSystem and database type session are supported.");
			}
			$this->start($this->_sessionId);
		}

		/**
		 * Returns the value associated with the given key. Thows exception if key is not set.
		 * @param  string $key 	The key to return
		 * @return mixed 	    The key value
		 */
		public function get(string $key):mixed
		{
			$return=$this->handler->getVar($key);
			if ($this->is_serial($return))
			{
				$return=unserialize(stripslashes($return));
			}
			return $return;
		}
		
		/**
		 * Sets the given value to the given key
		 * @param string $key name of the kety to set
		 * @param mixed  $val value for the key
		 * @return  bool true/false on success/failure
		 */
		public function set(string $key,mixed $val):bool //?mixed?
		{
			if (is_array($val))
			{
				return $this->handler->setVar($key,addslashes(serialize($val)));
			}
			else
			{
				return $this->handler->setVar($key,$val);
			}
		}

		/**
		 * Returns the value associated with the given key. Thows exception if key is not set.
		 * @param  string $key 	The key to return
		 * @return mixed 	    The key value
		 */
		public function __get(string $key):mixed
		{
			return $this->get($key);
		}
		
		/**
		 * Sets the given value to the given key
		 * @param string $key name of the kety to set
		 * @param mixed  $val value for the key
		 * @return  bool true/false on success/failure
		 */
		public function __set(string $key,mixed $val):bool
		{
			return $this->set($key, $val);
		}
		
		/**
		 * Checks if the given key exists in the session. To be used before calling __get to avoid exceptions.
		 * @param  string  $key the key name
		 * @return boolean      true/false if the key exists or not
		 */
		public function __isset(string $key):bool
		{
			return $this->handler->_isset($key);
		}
		
		/**
		 * Removes the given key from the session
		 * @param string $key the key to remove
		 * @return  bool  true/false on success failure
		 */
		public function delete(string $key):bool
		{
			return $this->handler->delete($key);
		}
		
		/**
		 * Status of the session
		 * @return bool true/false if started or not
		 */
		public function sessionStarted():bool
		{
			return $this->sessionStarted;
		}
		
		/**
		 * Returns the id for the current session.
		 * @return string the session id
		 */
		public function sessionId():string
		{
			if (is_null($this->_sessionId))
			{
				return $this->getSessionId();
			}
			else
			{
				return $this->_sessionId;
			}
		}
		
		/**
		 * Grabs the session name (id) from the cookie. Unless something went wrong the session name in the cookie should always be 
		 * the same as the session id.
		 * @return mixed the session name
		 */
		public function getSessionName():mixed
		{
			return $this->request->getCookie(self::SESSION_NAME);
		}
		
		private function is_serial(mixed $value):bool
		{
			$return=false;
			if (is_string($value))
			{
				if (preg_match('@a:\d+:\{@',$value))
				{
					if (is_array(unserialize($value)))$return=true;
				}
			}
			return $return;
		}
		
		/**
		 * Starts a new session
		 * @param  string|null $key optional, the id for the new session. if not given a random one will be generated
		 * @return bool          true/false on success/failure
		 */
		private function start(?string $key=null):bool
		{
			$return=false;
			if ($this->hasCookie() && !is_null($this->sessionId()))
			{
				$return=true;
			}
			else
			{
				if(is_null($key))
				{
					$key=$this->makeKey();
				}
				if ($this->createCookie($key))
				{
					if ($this->handler->insertNewSession($key))
					{
						$return=true;
					}
				}
			}
			$this->sessionStarted=$return;
			return $return;
		}
		
		/**
		 * Checks if there is a cookie for the nuclio session
		 * @return boolean true/false for existsing cookie or not
		 */
		private function hasCookie():bool
		{
			$map=$this->request->getCookie();
			if(!$map instanceof Map)
			{
				throw new SessionException("Something is wrong with the cookie.");
			}
			return $map->containsKey(self::SESSION_NAME);
		}
		
		/**
		 * Creates a cookie for the nuclio session with the given id
		 * @param  string $key the session id
		 * @return bool      true/false on success/failure
		 */
		private function createCookie(string $key):bool
		{
			return setcookie(self::SESSION_NAME,$key,0,'/');
		}
		
		/**
		 * Generates a random session id
		 * @return string key
		 */
		private function makeKey():string
		{
			$return=false;
			$ipElements	=(string)$this->request->getServer('REMOTE_ADDR')
						|> explode('.',$$)
						|> implode('',$$);
			$keyElements=[];
			sscanf
			(
				$ipElements,
				'%2d%2d%2d%2d%2d%2d',
				&$keyElements[0],
				&$keyElements[1],
				&$keyElements[2],
				&$keyElements[3],
				&$keyElements[4],
				&$keyElements[5]
			);
			if (!end(&$keyElements))
			{
				array_pop(&$keyElements);
			}
			for ($i=0; $i<count($keyElements); $i++)
			{
				$keyElements[$i]=chr($keyElements[$i]);
			}
			for ($i=0; $i<count($keyElements); $i++)
			{
				$keyElements[$i]=ord($keyElements[$i]);
			}
			$key=array();
			for ($i=0; $i<(int)$this->__sessionKey['length']; $i++)
			{
				$position=mt_rand(0,strlen($this->__sessionKey['chars']));
				$key[]=substr($this->__sessionKey['chars'],($position-1),1);
			}
			$return=implode('',$key);
			return $return;
		}
		
		/**
		 * Inserts a new session with the given key using the handler (either creates a file or a database record)
		 * @param  string $key the session key
		 * @return bool      true/false on success/failure
		 */
		private function insertNewSession(string $key):bool
		{
			return $this->handler->insertNewSession($key);
		}
		
		/**
		 * checks if the current session is expired
		 * @param  int|integer $expireTime optional expiration time, if omitted or 0 the default one will be used
		 * @return boolean                 true/false for expired or not
		 */
		public function isExpired(int $expireTime=0):bool
		{
			return $this->handler->isExpired($expireTime);
		}
		
		/**
		 * Updates the session time to the current time
		 * @return bool true/false on success/failure
		 */
		public function updateTouched():bool
		{
			return $this->handler->updateTouched();
		}
		
		/**
		 * Returns the current session id. if the id property is not set, tryies to grab the id from the cookies. 
		 * If no session cookie exists, creates a new key.
		 * @return string the current session id
		 */
		private function getSessionId():string
		{
			if (is_null($this->_sessionId))
			{
				$cookie = $this->request->getCookie();
				if(!$cookie instanceof Map)
				{
					throw new SessionException("Something is wrong with the cookie");
				}
				if($cookie->containsKey(self::SESSION_NAME))
				{
					return (string)$cookie->get(self::SESSION_NAME);
				}
				else
				{
					return $this->_sessionId = $this->makeKey();
				}
			}
			else
			{
				return $this->_sessionId;
			}
		}
		
		/**
		 * Returns the session expiration time. Set in the constructor, defaults to 3600
		 * @return int session expiration time
		 */
		public function getExpireTime():int
		{
			return $this->handler->getExpireTime();
		}
	}
}
